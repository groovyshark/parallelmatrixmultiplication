/*
 * source.h
 *
 *  Created on: 25 дек. 2014 г.
 *      Author: forelock
 */

#ifndef SOURCE_H_
#define SOURCE_H_

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/un.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>
#include <linux/if.h>
#include <ifaddrs.h>
#include <time.h>
#include <sstream>
#include <fstream>
#include <set>
#include <map>
#include <iostream>
#include <algorithm>

#define INVALID_SOCKET -1
#define MASTER "-m"
#define SLAVE  "-s"

void MatrixMultiplication(int**, int**, int**, int , int );

int VectorMultiplication(int*, int*, int);

int ProcessSlave(int);
//void WriteMatrixInFile(std::string, const Mat&);
//void ReadMatrixFromFile(std::string, Mat& );


#endif 
