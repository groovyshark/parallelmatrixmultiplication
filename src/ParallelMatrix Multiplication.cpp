//============================================================================
// Name        : ParallelMatrix.cpp
// Author      : Vyacheslav Rzhevskiy
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include "source.h"

const int port = 8073;
const int sizeMatrix = 10;

const char* fName1 = "/home/forelock/Eclipse/Projects/ParallelMatrixMultiplication/matrix1.bin";
const char* fName2 = "/home/forelock/Eclipse/Projects/ParallelMatrixMultiplication/matrix2.bin";

using namespace std;


class Mat
{
public:
    
    Mat():
        rows(0),
        cols(0),
        elSize(0),
        data(NULL)
    {

    }
    
    Mat(int _rows, int _cols, int _elSize)
        :rows(_rows)
        ,cols(_cols)
        ,elSize(_elSize)

    {
        data = new unsigned char [ size()];
        memset((void*)data,0, size());
    }
    ~Mat()
    {
        delete[] data;
    }

    Mat(const Mat& src)
    {
        rows = src.rows;
        cols = src.cols;
        elSize = src.elSize;
        data =  new unsigned char[src.size()];
        memcpy(data, src.data, src.size());
    }
   
    void operator =(const Mat& src)
    {
		if (this != &src)
		{
			rows = src.rows;
			cols = src.cols;
			elSize = src.elSize;
			delete[] data;
			data =  new unsigned char[src.size()];
			memcpy(data, src.data, src.size());
		}
      	else
        {
            throw std::runtime_error("Сам дурак!");
        }
    }
    
    template<typename T>
    T& at(int i, int j)
    {
        return ((T*)(data + i*elSize*cols))[j];
        
    }

    template<typename T>
    const T& at(int i, int j)const
    {
        return ((T*)(data + i * elSize * cols))[j];
    }
    
    //Взять указатель на строку
    template<typename T>
    T* ptr(int i)
    {
        return ((T*)(data)) + i * cols;
    }
        //Взять указатель на строку
    template<typename T>
    const T* ptr(int i)const
    {
        return ((T*)(data)) + i * cols;
    }
    
    int size()const
    {
       return rows * cols * elSize; 
    }
    template<typename T>
    Mat transpose()
    {
        Mat m(cols, rows, elSize);

        for(int i = 0; i < cols; ++i)
        {
            for (int j = 0; j < rows; ++j)
            {
                m.at<T>(i, j) = at<T>(j, i);
            }
        }
        
        return m;
    }
    
    int rows;
    int cols;
    int elSize;
private:
    unsigned char* data;
};

void WriteMatrixInFile(std::string, const Mat&);
void ReadMatrixFromFile(std::string, Mat& );
void CalcMatrixRow(const Mat& ,const Mat& , double* , int);
void InitMatrix(Mat&, Mat&);
void PrintMatrix(Mat&);

int main(int argc, char* argv[])
{
	int numClients;
    
    Mat A,B,C;
    
	int S;  //дескриптор прослушивающего сокета
	int  NS; //дескриптор присоединенного сокета

	sockaddr_in serv_addr;
	char  sName[128];
	char buf[1024];
	int bytes_read;

	gethostname(sName, sizeof(sName));
	printf("\nServer host: %s\n", sName);

	if ((S = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
	{
		fprintf(stderr, "Can't create socket\n");
		exit(1);
	}

	// Заполняем структуру адресов
	if (strcmp(argv[1], MASTER) == 0)
	{
	    if (argc < 5)
        {
            throw std::runtime_error("Bad command line parametems");
        }
       
        int nRows = atoi(argv[2]);
        int nCols = atoi(argv[3]);
        numClients = atoi(argv[4]);
        
        A = Mat(nRows, nCols, sizeof(double));
        B = Mat(nRows, nCols, sizeof(double));
        C = Mat(nRows, nCols, sizeof(double));
        
		fcntl(S, F_SETFL, O_NONBLOCK);

		memset(&serv_addr, 0, sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_addr.s_addr = INADDR_ANY;
		serv_addr.sin_port = htons((u_short)port);

		// Связываем сокет с заданным сетевым интерфесом и портом
		if (bind(S, (sockaddr*)&serv_addr, sizeof(serv_addr)) == INVALID_SOCKET)
		{
			perror("Can't bind\n");
			exit(1);
		}

		// Переводим сокет в режим прослушивания заданного порта
		// с максимальным количеством ожидания запросов на соединение 5
		if (listen(S, 5) == INVALID_SOCKET)
		{
			perror("Can't listen\n");
			exit(1);
		}

		printf("Master listen on %s:%d\n",
			inet_ntoa(serv_addr.sin_addr), ntohs(serv_addr.sin_port));

		sockaddr_in clnt_addr;
		int addrlen = sizeof(clnt_addr);
		memset(&clnt_addr, 0, sizeof(clnt_addr));



		set <int> clients;
		map <int, bool> clientsReady;
		clients.clear();

		InitMatrix(A, B);

		PrintMatrix(A);
		cout<<endl;
		cout<<endl;
		PrintMatrix(A);
        
        WriteMatrixInFile(fName1, A);
        WriteMatrixInFile(fName2, B);

        cout<<"Size Matrix A: "<<A.size()<<endl;
        cout<<"Size Matrix B: "<<B.size()<<endl;

		printf("Wait for connections.....\n");
		fd_set readset;
		while(1)
		{
			// Заполняем множество сокетов
			FD_ZERO(&readset);
			FD_SET(S, &readset); 

			for(auto it = clients.begin(); it != clients.end(); it++)
				FD_SET(*it, &readset);
				
			// Задаём таймаут
			timeval timeout;
			timeout.tv_sec = 30;
			timeout.tv_usec = 0;

			// Ждём события в одном из сокетов
			int mx = max(S, *max_element(clients.begin(), clients.end()));
			if(select(mx+1, &readset, NULL, NULL, &timeout) <= 0)
			{
				perror("select");
				exit(3);
			}

			// Определяем тип события и выполняем соответствующие действия
			if(FD_ISSET(S, &readset))
			{
				// Поступил новый запрос на соединение, используем accept
				int sock = accept(S, NULL, NULL);
				if(sock < 0)
				{
					perror("accept");
					exit(3);
				}

				fcntl(sock, F_SETFL, O_NONBLOCK);

				clients.insert(sock);
			}

            
			for(auto it = clients.begin(); it != clients.end(); it++)
			{
				if(FD_ISSET(*it, &readset))
				{
					bytes_read = recv(*it, buf, 1024, 0);
					if(bytes_read <= 0)
					{
						// Соединение разорвано, удаляем сокет из множества
						close(*it);
						clients.erase(*it);
						continue;
					}
					if (strcmp(buf, "fname1") == 0)
					{
					    send(*it, fName1, 1024, 0);
					}
					if (strcmp(buf, "fname2") == 0)
					{
					    send(*it, fName2, 1024, 0);
					}
					if (strcmp(buf, "Matrix readed") == 0)
					{
					    clientsReady[*it] = true;
					}
					
				}
			}
			bool allReady = false;
			int numReady = 0;
			for(auto it = clientsReady.begin(); it != clientsReady.end(); ++it)
			{
			    if(it->second == true)
			    {
                    numReady++;
			    }
			}
			if(numReady == numClients)
			    break;
		}
		cout<<"All clients is ready"<<endl;
	
		vector <int> clientrow(numClients, -1);
		vector <int> clientsV(numClients, 0);
		vector <int> corruptedIndx;

		int currentRow = 0;
		for(auto it = clients.begin(); it != clients.end(); it++)
		{
		    clientsV[currentRow] = *it;
		    clientrow[currentRow] = currentRow;
		    
		    send(*it, &currentRow, 4, 0);
		    currentRow++;
		}
		
		int numCalcRows = 0;
		int rowSz = C.cols * C.elSize;
		cout<<"Computing start..."<<endl;
		while(numCalcRows < C.rows)
		{
			// Заполняем множество сокетов
			FD_ZERO(&readset);
			FD_SET(S, &readset);

			for(auto it = clientsV.begin(); it != clientsV.end(); it++)
				FD_SET(*it, &readset);
				
			// Задаём таймаут
			timeval timeout;
			timeout.tv_sec = 30;
			timeout.tv_usec = 0;

			// Ждём события в одном из сокетов
			int mx = max(S, *max_element(clientsV.begin(), clientsV.end()));
			if(select(mx+1, &readset, NULL, NULL, &timeout) <= 0)
			{
				perror("select");
				exit(3);
			}
		    
		    for(int i = 0; i < clientsV.size(); ++i)
			{
		        if(FD_ISSET(clientsV[i], &readset))
				{
					bytes_read = recv(clientsV[i], C.ptr<double>(clientrow[i]), 1024, 0);
					cout<<"bytes_read: "<<bytes_read<<endl;
					if(bytes_read <= 0 || bytes_read != rowSz)
					{
						// Соединение разорвано, удаляем сокет из множества
						close(clientsV[i]);
						clients.erase(clientsV[i]);
						
						corruptedIndx.push_back(clientrow[i]); 
					}
					
					if(!corruptedIndx.empty())
					{
					    send(clientsV[i], &corruptedIndx[0], 4, 0);
					    corruptedIndx.erase(corruptedIndx.begin());
					}
					
					if(currentRow < C.rows)
					{
					    send(clientsV[i], &currentRow, 4, 0);
					    clientrow[i] = currentRow;
		                currentRow++;
					}

		            numCalcRows++;
				}
			}
		}

		cout<<"Result matrix: "<<endl;
		PrintMatrix(C);

	}
	else if (strcmp(argv[1], SLAVE) == 0)
	{
		memset(&serv_addr, 0, sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_addr.s_addr =  inet_addr("127.0.0.1");
		serv_addr.sin_port = htons((u_short)port);

		if (connect(S,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
		{
			perror("Can't connect");
			exit(1);
		}
		cout<<"Connected!"<<endl;
		
		send(S, "fname1", strlen("fname1"), 0);
		
		char buf[1024];
		recv(S, buf, 1024, 0);
		cout<<buf<<endl;
		ReadMatrixFromFile(buf, A);
		
		sprintf(buf, "fname2");
		send(S, buf, 1024, 0);
		
		recv(S, buf, 1024, 0);
		cout<<buf<<endl;
		ReadMatrixFromFile(buf, B);
	
		B = B.transpose<double>();
		cout<<"Transposed!"<<endl;
		
		PrintMatrix(A);
		cout<<endl;
		//cout<<endl;
		PrintMatrix(B);

		cout<<"Size Matrix A: "<<A.size()<<endl;
		cout<<"Size Matrix B: "<<B.size()<<endl;

		sprintf(buf, "Matrix readed");
		send(S, buf, 1024, 0);
		
		int rowSz = A.cols * A.elSize;
	    unsigned char* matrix_row[rowSz];
	    while(true)
	    {
	        bytes_read = recv(S, buf, 1024, 0);
	        //cout<<buf<<endl;
	        if(bytes_read != 4)
	        { 
	        	cout<<"bytes_read: "<<bytes_read<<endl;
	            if(bytes_read <= 0)
	            {
	                cout<<"Disconnect..."<<endl;
	                close(S);
	                break;
	            }
	            
	            cout<<"Wrong size"<<endl;
	            send (S, "Wrong size", strlen("Wrong size"), 0);
	            continue;
	        }
	        
	        int indx = ((int*)buf)[0];
	        
	        cout<<"Index: "<<indx<<endl;

	        if(indx > A.cols)
	        {
	            cout<<"Wrong index"<<endl;
	            send (S, "Wrong index", strlen("Wrong index"), 0);
	        }
            if(indx < 0)	        
            {
                close(S);
                //заканчиваем программу отключаемся
                break;
            }
            
	        CalcMatrixRow(A, B, (double*)matrix_row, indx);
	        send (S, matrix_row, rowSz, 0);
	        
	    }
	    
	}

	return 0;
}

void PrintMatrix(Mat& A)
{
	for(int i = 0; i < A.rows; ++i)
	{
		for(int j = 0; j < A.cols; ++j)
		{
			cout<<A.at<double>(i, j)<<" ";
		}
		cout<<endl;
	}
}


void CalcMatrixRow(const Mat& A,const Mat& B, double* resMatrixrow, int indx)
{
    const double* adata = A.ptr<double>(indx);
    const double* bdata = B.ptr<double>(0);
    int step = A.cols;
    
    for(int i = 0; i < B.rows; ++i)
    {
        resMatrixrow[i] = 0.0;
        for(int j = 0; j < B.cols; ++j)
        {
            resMatrixrow[i] += adata[j] * bdata[j];
        }
        cout<<resMatrixrow[i]<<" ";
        
        bdata +=step;
    }
    cout<<endl;
    //Конец
}


void InitMatrix(Mat& A, Mat& B)
{
	srand (time(NULL));

    if(A.rows == 0 || B.rows == 0)
        throw std::runtime_error("Matrix empty");

	for(int i = 0; i < A.rows; ++i)
	{
		for(int j = 0; j < B.cols; ++j)
		{
			A.at<double>(i,j) = j * j;
			B.at<double>(i,j) = j * j;
			
		}
	}

}


void WriteMatrixInFile(string filename, const Mat& m)
{
	ofstream fout;

	fout.open(filename, ios::binary | ios::out | ios::trunc);
    
    fout.write((char*)&m.rows, 4);
    fout.write((char*)&m.cols, 4);
    fout.write((char*)&m.elSize, 4);
    
    fout.write(m.ptr<char>(0), m.size());

    std::cout << "File: " << filename << std::endl 
    << "Write mat:" << std::endl 
            << "rows:" << m.rows << std::endl
            << "cols:" << m.cols << std::endl
            << "elSize:" << m.elSize << std::endl;

    
	fout.close();
}

void ReadMatrixFromFile(string filename, Mat& m)
{
	ifstream fin;
	fin.open(filename, ios::binary | ios::in);
	
	if(!fin.is_open())
	    throw std::runtime_error("Can't open file");

	int rows,cols,elSize;
	
	
	fin.readsome((char*)&rows, 4);
	fin.readsome((char*)&cols, 4);
	fin.readsome((char*)&elSize, 4);

    std::cout << "File: " << filename << std::endl
                << "rows:" << rows << std::endl
                << "cols:" << cols << std::endl
                << "elSize:" << elSize << std::endl;
                
    m = Mat(rows,cols,elSize);

    int nBytes = fin.readsome(m.ptr<char>(0), m.size());

    std::cout <<  "Bytes read:" << nBytes << std::endl;
    
    fin.close();
}
